from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(ModelForm):
    #Here you could hypothetically add an email address that people could go to send a recipe to someone as an email.
    #email_address = forms.EmailField(max_length=300)
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]
