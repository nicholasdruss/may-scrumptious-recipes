from django.urls import path
from recipes.views import recipe_list, show_recipe, create_recipe, edit_post, my_recipe_list
# from django.http import HttpResponse

urlpatterns = [
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("", recipe_list, name="recipe_list"),
    path("<int:id>/edit", edit_post, name="edit_post"),
    path("mine/", my_recipe_list, name="my_recipe_list")
]
