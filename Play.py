from django.shortcuts import render, get_object_or_404, redirect
from <path to models(recipes.models)> import <Class name>
from <path to forms(recipes.forms)> import <FormName>

def <Function Name>(request, id):
    <Variable> = get_object_or_404(<Model Class>, id=id)
    if request.method == "POST":
        <Variable for Form(ex form)> = <Form Name>(request.POST, instance=<Variable>)
        if <Form Variable>.is_valid():
            <Form Variable>.save()
            return redirect("show_recipe", id=id)
            # IF the form is Valid, we save it and then redirect
            # Back to the main page.
    else:
        form = RecipeForm(instance=<Same Variable created above>)
    context = {
        "<Any Context name(ex post_object)": <Variable created above>,
        "<Any context name(ex post_form)>": <Variable we created for the Form in Forms.py>,
    }
    return render(request, "<URL Path for our Edit Page>", context)

# Instance is the the form that has the changes
# Request in render tells it to render it to the request made, path it needs to go
# Call in the context